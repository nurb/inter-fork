<?php


namespace App\Controller;


use App\Repository\VideoGalleryModuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class VideoGalleryModuleController extends Controller
{
    /**
     * @Route("/videogallery/slider", name="app_videogallery_slider")
     * @param VideoGalleryModuleRepository $videos
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function  showVideoGallerySliderAction(VideoGalleryModuleRepository $videos){

        $videos = $videos->getIsOnMainPage();

        return $this->render('videogallery_module/slider_videogallery.html.twig',[
            'videos' => $videos
        ]);
    }
    /**
     * @Route("/videogallery", name="app_videogallery_all")
     * @param VideoGalleryModuleRepository $videoGalleryRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAllVideoGalleryAction(VideoGalleryModuleRepository $videoGalleryRepository)
    {
        $videos = $videoGalleryRepository->findAll();

        return $this->render('videogallery_module/page_video.html.twig', [
            'videos' => $videos
        ]);
    }

}