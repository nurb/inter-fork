<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 03.09.18
 * Time: 19:52
 */

namespace App\EventSubscriber;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;



class SiteSettingsSubscriber extends Controller implements EventSubscriberInterface
{
    private $em;
    private $session;

    public function __construct(EntityManager $em, Session $session)
    {
        $this->em = $em;
        $this->session = $session;
    }

    public function onKernelController(FilterControllerEvent $event)
    {

        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        $settings = $this->em->getRepository('App:SiteSettings')->find(1);


            $this->session->set('mainPageTitle', $settings->getMainPageTitle());
            $this->session->set('mainPageDescription', $settings->getMainPageDescription());
            $this->session->set('mainPageTextBlock', $settings->getMainPageTextBlock());
            $this->session->set('mainSliderTimer', $settings->getMainSliderTimer());
            $this->session->set('coachSliderTimer', $settings->getCoachSliderTimer());
            $this->session->set('reviewSliderTimer', $settings->getReviewSliderTimer());
            $this->session->set('videoGallerySliderTimer', $settings->getVideoGallerySliderTimer());
            $this->session->set('eventsSliderTimer', $settings->getEventsSliderTimer());
            $this->session->set('fc', $settings->getFacebookUrl());
            $this->session->set('is', $settings->getInstagramUrl());
            $this->session->set('go',$settings->getGoogleUrl());
            $this->session->set('vk',$settings->getVkontakteUrl());


    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
        );
    }

}