<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 20.07.18
 * Time: 18:37
 */

namespace App\Entity;


use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SliderModuleRepository")
 * @Vich\Uploadable
 *
 */
class SliderModule
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;


    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern     = "/^(https?:\/\/)?([\w\.]+)\.([a-z]{2,6}\.?)(\/[\w\.]*)*\/?$/",
     *     message="Извините, неверный формат URL"
     * )
     *
     * @ORM\Column(type="string")
     */
    private $link;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     *
     */
    private $isActive;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="slides", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    // ...

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return SliderModule
     */
    public function setName(string $name): SliderModule
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $text
     * @return SliderModule
     */
    public function setText(string $text): SliderModule
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @param string $link
     * @return SliderModule
     */
    public function setLink(string $link): SliderModule
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @param bool $isActive
     * @return SliderModule
     */
    public function setIsActive(bool $isActive): SliderModule
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @return bool
     */
    public function isActive(): ?bool
    {
        return $this->isActive;
    }
}