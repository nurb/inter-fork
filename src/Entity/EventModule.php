<?php


namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventModuleRepository")
 * @Vich\Uploadable
 */
class EventModule
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $eventName;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $eventImage;

    /**
     * @var File
     * @Vich\UploadableField(mapping="event_photo", fileNameProperty="eventImage")
     */
    private $imageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $shortDescription;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $fullDescription;

    /**
     *
     * @Assert\Expression(
     *     "this.getEventStart() <= this.getEventEnding()",
     *      message="Дата начала мероприятия должна быть больше даты окончания"
     * )
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    private $eventStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    private $eventEnding;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $priority;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $orderLimit;


    /**
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\EventCategory", inversedBy="events")
     */
    private $categories ;


    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $eventType;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CoachModule", mappedBy="events")
     */
    private $eventCoachs;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EventSession", mappedBy="event", cascade={"persist", "remove"})
     */
    private $eventSessions;


    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->eventSessions = new ArrayCollection();
        $this->eventCoachs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEventName(): ?string
    {
        return $this->eventName;
    }

    public function setEventName(string $eventName): self
    {
        $this->eventName = $eventName;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventImage()
    {
        return $this->eventImage;
    }

    public function setEventImage($eventImage): self
    {
        $this->eventImage = $eventImage;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(string $shortDescription): self
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    public function getFullDescription(): ?string
    {
        return $this->fullDescription;
    }

    public function setFullDescription(?string $fullDescription): self
    {
        $this->fullDescription = $fullDescription;

        return $this;
    }

    public function getEventStart(): ?\DateTimeInterface
    {
        return $this->eventStart;
    }

    public function setEventStart(?\DateTimeInterface $eventStart): self
    {
        $this->eventStart = $eventStart;

        return $this;
    }

    public function getEventEnding(): ?\DateTimeInterface
    {
        return $this->eventEnding;
    }

    public function setEventEnding(?\DateTimeInterface $eventEnding): self
    {
        $this->eventEnding = $eventEnding;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getEventType(): ?string
    {
        return $this->eventType;
    }

    public function setEventType(string $eventType): self
    {
        $this->eventType = $eventType;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories ;
    }


    public function addCategory(EventCategory $category):self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addEvent($this);
        }

        return $this;
    }



    /**
     * @return ArrayCollection
     */
    public function getEventCoachs()
    {
        return $this->eventCoachs;
    }

    public function addEventCoach(CoachModule $eventCoach): self
    {
        if (!$this->eventCoachs->contains($eventCoach)) {
            $this->eventCoachs[] = $eventCoach;
            $eventCoach->addEvent($this);
        }

        return $this;
    }

    public function removeEventCoach(CoachModule $eventCoach): self
    {
        if ($this->eventCoachs->contains($eventCoach)) {
            $this->eventCoachs->removeElement($eventCoach);
            $eventCoach->removeEvent($this);
        }

        return $this;
    }

    /**
     * @return ArrayCollection|EventSession[]
     */
    public function getEventSessions()
    {
        return $this->eventSessions;
    }

    public function addEventSession(EventSession $eventSession): self
    {
        if (!$this->eventSessions->contains($eventSession)) {
            $this->eventSessions[] = $eventSession;
            $eventSession->setEvent($this);
        }

        return $this;
    }

    public function removeEventSession(EventSession $eventSession): self
    {
        if ($this->eventSessions->contains($eventSession)) {
            $this->eventSessions->removeElement($eventSession);
            // set the owning side to null (unless already changed)
            if ($eventSession->getEvent() === $this) {
                $eventSession->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @param File $imageFile
     * @return EventModule
     */
    public function setImageFile(File $imageFile): EventModule
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return $this->eventName ?? '';
    }

    /**
     * @param int $orderLimit
     * @return EventModule
     */
    public function setOrderLimit(int $orderLimit): EventModule
    {
        $this->orderLimit = $orderLimit;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderLimit(): ?int
    {
        return $this->orderLimit;
    }


}