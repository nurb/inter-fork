<?php

namespace App\Entity;


use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContext;

/**
 * @Gedmo\Tree(type="nested")
 * use repository for handy tree functions
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\NestedTreeRepository")
 */
class DynamicPage
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="Длина заголовка страницы должна быть не менее 3 символов",
     *     minMessage="Длина заголовка страницы должна быть не более 255 символов",
     * )
     * @ORM\Column(type="string", unique=true)
     */
    private $title;

    /**
     *
     * @var string
     *
     * @Assert\Regex(
     *     pattern     = "/^[a-zA-Z-0-9]+$/",
     *     message="Извините, доступны только цифры и символы латинского алфавита"
     * )
     *
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="Длина адресного имени должна быть не менее 3 символов",
     *     minMessage="Длина адресного имени должна быть не более 255 символов",
     * )
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $route;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $isAvailable;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $isShowOnMenu;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $pageModule;

    /**
     * @var string
     *
     * @ORM\Column(type="string",length=1024)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(type="string",length=1024)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(type="string",length=1024)
     */
    private $metaH1;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer", name="left_tree")
     */
    private $left;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer", name="right_tree")
     */
    private $right;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="DynamicPage")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;


    /**
     * @var DynamicPage
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="DynamicPage", inversedBy="children")
     *
     */
    private $parent;

    /**
     * @var PersistentCollection
     *
     * @ORM\OrderBy({"left" = "ASC"})
     * @ORM\OneToMany(targetEntity="DynamicPage", mappedBy="parent",cascade={"persist", "remove"})
     * @JoinColumn(name="parent_id", referencedColumnName="id")*
     */
    private $children;

    private $leveledTitle;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $title
     * @return DynamicPage
     */
    public function setTitle(string $title): DynamicPage
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $route
     * @return DynamicPage
     */
    public function setRoute(string $route): DynamicPage
    {
        $this->route = $route;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @param bool $isAvailable
     * @return DynamicPage
     */
    public function setIsAvailable(bool $isAvailable): DynamicPage
    {
        $this->isAvailable = $isAvailable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailable(): ?bool
    {
        return $this->isAvailable;
    }

    /**
     * @param bool $isShowOnMenu
     * @return DynamicPage
     */
    public function setIsShowOnMenu(bool $isShowOnMenu): DynamicPage
    {
        $this->isShowOnMenu = $isShowOnMenu;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowOnMenu(): ?bool
    {
        return $this->isShowOnMenu;
    }

    /**
     * @param mixed $pageModule
     * @return DynamicPage
     */
    public function setPageModule(?string $pageModule): DynamicPage
    {
        $this->pageModule = $pageModule;
        return $this;
    }

    /**
     * @return string
     */
    public function getPageModule(): ?string
    {
        return $this->pageModule;
    }

    /**
     * @param string $metaKeywords
     * @return DynamicPage
     */
    public function setMetaKeywords(string $metaKeywords): DynamicPage
    {
        $this->metaKeywords = $metaKeywords;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaDescription
     * @return DynamicPage
     */
    public function setMetaDescription(string $metaDescription): DynamicPage
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaTitle
     * @return DynamicPage
     */
    public function setMetaTitle(string $metaTitle): DynamicPage
    {
        $this->metaTitle = $metaTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * @param string $metaH1
     * @return DynamicPage
     */
    public function setMetaH1(string $metaH1): DynamicPage
    {
        $this->metaH1 = $metaH1;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaH1(): ?string
    {
        return $this->metaH1;
    }

    /**
     * @param DynamicPage $parent
     * @return DynamicPage
     */
    public function setParent(DynamicPage $parent): DynamicPage
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return DynamicPage
     */
    public function getParent(): ?DynamicPage
    {
        return $this->parent;
    }


    /**
     * @param string $content
     * @return DynamicPage
     */
    public function setContent(?string $content): DynamicPage
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @return PersistentCollection
     */
    public function getChildren(): PersistentCollection
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getFullRoute(): string
    {
        $page = $this;
        $routes[] = $this->getRoute();
        while ($page->getParent()->getLevel() != 0) {
            $routes[] = $page->getParent()->getRoute();
            $page = $page->getParent();
        }
        return implode('/', array_reverse($routes));
    }

    /**
     * @return array
     */
    public function getBreadCamps(): array
    {
        $page = $this;
        $breadCamps = [];
        while ($page->getParent()->getLevel() != 0) {
            $breadCamps[] = ["route" => $page->getParent()->getFullRoute(), "title" => $page->getParent()->getTitle()];
            $page = $page->getParent();
        }
        return array_reverse($breadCamps);
    }

    /**
     * @param mixed $left
     * @return DynamicPage
     */
    public function setLeft($left)
    {
        $this->left = $left;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     * @return DynamicPage
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @param mixed $root
     * @return DynamicPage
     */
    public function setRoot($root)
    {
        $this->root = $root;
        return $this;
    }

    /**
     * @param mixed $right
     * @return DynamicPage
     */
    public function setRight($right = 0)
    {
        $this->right = $right;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @return mixed
     */
    public function getRight()
    {
        return $this->right;
    }

    public function getLeveledTitle()
    {
        if ($this->level - 1 < 0) {
            return $this->title;
        }
        return str_repeat("—", $this->level - 1) . " " . $this->title;
    }
}