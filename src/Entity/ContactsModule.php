<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactsModuleRepository")
 *
 */
class ContactsModule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $fax;

    /**
     * @Assert\Regex(
     *     pattern="/\d+/",
     *     match=true,
     *     message="Поле должно содержать только цифры"
     * )
     *
     * @var string
     *
     *
     * @ORM\Column(type="text")
     */
    private $phones;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $address;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $coordinate;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $phonesForViewSonata;

    public $map;

    public function getId()
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $fax
     * @return ContactsModule
     */
    public function setFax(string $fax): ContactsModule
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * @return string
     */
    public function getFax(): ?string
    {
        return $this->fax;
    }


    /**
     * @param string $address
     * @return ContactsModule
     */
    public function setAddress(string $address): ContactsModule
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $coordinate
     * @return ContactsModule
     */
    public function setCoordinate(string $coordinate): ContactsModule
    {
        $this->coordinate = $coordinate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCoordinate(): ?string
    {
        return $this->coordinate;
    }

    /**
     *
     * @return mixed (Phone|string)[]
     */
    public function getPhones()
    {
        return json_decode($this->phones, true);
    }



    public function setPhones(array $phones)
    {
        $this->phonesForViewSonata = json_encode($phones);
        $this->phones = json_encode($phones);
        return $this;
    }

    /**
     * @return string
     */
    public function getPhonesForViewSonata()
    {

       $phones = json_decode($this->phonesForViewSonata,true);
       $arrayToString = implode("\n\n",$phones);
           return $arrayToString;
    }
}
