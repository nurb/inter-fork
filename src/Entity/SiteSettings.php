<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 27.08.18
 * Time: 19:21
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\SiteSettingsRepository")
 */
class SiteSettings
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=128)
     */
    private $mainPageTitle;

    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $mainPageDescription;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $mainSliderTimer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $coachSliderTimer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $reviewSliderTimer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $videoGallerySliderTimer;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $eventsSliderTimer;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=4096)
     */
    private $mainPageTextBlock;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $adminEmail;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $facebookUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $instagramUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $googleUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $vkontakteUrl;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern="/^[0-9]+[x][0-9]+$/",
     *     message="Неверный формат.")
     */
    private $imageSizeLevel1;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern="/^[0-9]+[x][0-9]+$/",
     *     message="Неверный формат.")
     */
    private $imageSizeLevel2;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern="/^[0-9]+[x][0-9]+$/",
     *     message="Неверный формат.")
     */
    private $imageSizeLevel3;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern="/^[0-9]+[x][0-9]+$/",
     *     message="Неверный формат.")
     */
    private $imageSizeLevel4;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern="/^[0-9]+[x][0-9]+$/",
     *     message="Неверный формат.")
     */
    private $imageSizeLevel5;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $mainPageTitle
     * @return SiteSettings
     */
    public function setMainPageTitle(string $mainPageTitle): SiteSettings
    {
        $this->mainPageTitle = $mainPageTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainPageTitle()
    {
        return $this->mainPageTitle;
    }

    /**
     * @param string $mainPageDescription
     * @return SiteSettings
     */
    public function setMainPageDescription(string $mainPageDescription): SiteSettings
    {
        $this->mainPageDescription = $mainPageDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainPageDescription()
    {
        return $this->mainPageDescription;
    }

    /**
     * @param int $mainSliderTimer
     * @return SiteSettings
     */
    public function setMainSliderTimer(int $mainSliderTimer): SiteSettings
    {
        $this->mainSliderTimer = $mainSliderTimer;
        return $this;
    }

    /**
     * @return int
     */
    public function getMainSliderTimer()
    {
        return $this->mainSliderTimer;
    }

    /**
     * @param int $coachSliderTimer
     * @return SiteSettings
     */
    public function setCoachSliderTimer(int $coachSliderTimer): SiteSettings
    {
        $this->coachSliderTimer = $coachSliderTimer;
        return $this;
    }

    /**
     * @return int
     */
    public function getCoachSliderTimer()
    {
        return $this->coachSliderTimer;
    }

    /**
     * @param int $reviewSliderTimer
     * @return SiteSettings
     */
    public function setReviewSliderTimer(int $reviewSliderTimer): SiteSettings
    {
        $this->reviewSliderTimer = $reviewSliderTimer;
        return $this;
    }

    /**
     * @return int
     */
    public function getReviewSliderTimer()
    {
        return $this->reviewSliderTimer;
    }

    /**
     * @param int $eventsSliderTimer
     * @return SiteSettings
     */
    public function setEventsSliderTimer(int $eventsSliderTimer): SiteSettings
    {
        $this->eventsSliderTimer = $eventsSliderTimer;
        return $this;
    }

    /**
     * @return int
     */
    public function getEventsSliderTimer()
    {
        return $this->eventsSliderTimer;
    }

    /**
     * @param string $mainPageTextBlock
     * @return SiteSettings
     */
    public function setMainPageTextBlock(string $mainPageTextBlock): SiteSettings
    {
        $this->mainPageTextBlock = $mainPageTextBlock;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainPageTextBlock()
    {
        return $this->mainPageTextBlock;
    }

    /**
     * @param string $adminEmail
     * @return SiteSettings
     */
    public function setAdminEmail(string $adminEmail): SiteSettings
    {
        $this->adminEmail = $adminEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdminEmail(): string
    {
        return $this->adminEmail;
    }

    /**
     * @param string $facebookUrl
     * @return SiteSettings
     */
    public function setFacebookUrl(string $facebookUrl): SiteSettings
    {
        $this->facebookUrl = $facebookUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookUrl(): string
    {
        return $this->facebookUrl;
    }

    /**
     * @param string $instagramUrl
     * @return SiteSettings
     */
    public function setInstagramUrl(string $instagramUrl): SiteSettings
    {
        $this->instagramUrl = $instagramUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstagramUrl(): string
    {
        return $this->instagramUrl;
    }

    /**
     * @param string $imageSizeLevel1
     * @return SiteSettings
     */
    public function setImageSizeLevel1(string $imageSizeLevel1): SiteSettings
    {
        $this->imageSizeLevel1 = $imageSizeLevel1;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageSizeLevel1(): string
    {
        return $this->imageSizeLevel1;
    }

    /**
     * @param string $imageSizeLevel2
     * @return SiteSettings
     */
    public function setImageSizeLevel2(string $imageSizeLevel2): SiteSettings
    {
        $this->imageSizeLevel2 = $imageSizeLevel2;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageSizeLevel2(): string
    {
        return $this->imageSizeLevel2;
    }

    /**
     * @param string $imageSizeLevel3
     * @return SiteSettings
     */
    public function setImageSizeLevel3(string $imageSizeLevel3): SiteSettings
    {
        $this->imageSizeLevel3 = $imageSizeLevel3;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageSizeLevel3(): string
    {
        return $this->imageSizeLevel3;
    }

    /**
     * @param string $imageSizeLevel4
     * @return SiteSettings
     */
    public function setImageSizeLevel4(string $imageSizeLevel4): SiteSettings
    {
        $this->imageSizeLevel4 = $imageSizeLevel4;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageSizeLevel4(): string
    {
        return $this->imageSizeLevel4;
    }

    /**
     * @param string $imageSizeLevel5
     * @return SiteSettings
     */
    public function setImageSizeLevel5(string $imageSizeLevel5): SiteSettings
    {
        $this->imageSizeLevel5 = $imageSizeLevel5;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageSizeLevel5(): string
    {
        return $this->imageSizeLevel5;
    }

    /**
     * @param int $videoGallerySliderTimer
     * @return SiteSettings
     */
    public function setVideoGallerySliderTimer(int $videoGallerySliderTimer): SiteSettings
    {
        $this->videoGallerySliderTimer = $videoGallerySliderTimer;
        return $this;
    }

    /**
     * @return int
     */
    public function getVideoGallerySliderTimer(): int
    {
        return $this->videoGallerySliderTimer;
    }

    /**
     * @param string $googleUrl
     * @return SiteSettings
     */
    public function setGoogleUrl(string $googleUrl): SiteSettings
    {
        $this->googleUrl = $googleUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleUrl(): string
    {
        return $this->googleUrl;
    }

    /**
     * @param string $vkontakteUrl
     * @return SiteSettings
     */
    public function setVkontakteUrl(string $vkontakteUrl): SiteSettings
    {
        $this->vkontakteUrl = $vkontakteUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getVkontakteUrl(): string
    {
        return $this->vkontakteUrl;
    }


}