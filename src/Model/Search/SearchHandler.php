<?php

namespace App\Model\Search;


use App\Entity\DynamicPage;
use App\Repository\DynamicPageRepository;
use App\Repository\EventModuleRepository;
use App\Repository\NewsModuleRepository;

class SearchHandler
{
    /**
     * @var EventModuleRepository
     */
    private $eventModuleRepository;
    /**
     * @var DynamicPageRepository
     */
    private $dynamicPageRepository;
    /**
     * @var NewsModuleRepository
     */
    private $newsModuleRepository;

    public function __construct(EventModuleRepository $eventModuleRepository,
                                DynamicPageRepository $dynamicPageRepository,
                                NewsModuleRepository $newsModuleRepository)
    {
        $this->eventModuleRepository = $eventModuleRepository;
        $this->dynamicPageRepository = $dynamicPageRepository;
        $this->newsModuleRepository = $newsModuleRepository;
    }

    /**
     * @param $word
     * @return array|null
     */
    public function searchInEventModule($word):? array
    {
        $eventResults = $this->eventModuleRepository->searchByWord($word);
        foreach ($eventResults as $key => $eventResult) {
            $eventResults[$key] = array_merge(['route' => 'app_events_single'], $eventResult);
        }
        return $eventResults;
    }

    /**
     * @param $word
     * @return array|null
     */
    public function searchInNewsModule($word):? array
    {
        $newsResults = $this->newsModuleRepository->searchByWord($word);
        foreach ($newsResults as $key => $newsResult) {
            $newsResults[$key] = array_merge(['route' => 'app_single_news'], $newsResult);
        }
        return $newsResults;
    }

    /**
     * @param $word
     * @return array|null
     */
    public function searchInDynamicPageModule($word):? array
    {
        $dynamicPageObjects = $this->dynamicPageRepository->searchByWord($word);
        $dynamicPageResults = [];
        foreach ($dynamicPageObjects as $key => $dynamicPageObject) {
            /** @var  $dynamicPageObject DynamicPage */
            $dynamicPageObject->getFullRoute();
            $dynamicPageResults[] =
                [
                    'route' => 'app-dynamic-page',
                    'id' => $dynamicPageObject->getFullRoute(),
                    'title' => $dynamicPageObject->getTitle(),
                    'shortDescription' => $dynamicPageObject->getContent()
                ];
        }
        return $dynamicPageResults;
    }
}