<?php

namespace App\Model\Enumeration;


use App\Repository\DynamicPageRepository;

class PageModulesEnumeration
{
    public const SLIDER = 'slider';
    public const EVENTS = 'events';
    public const COACH = 'coach';
    public const NEWS = 'news';
    public const REVIEWS = 'reviews';
    public const GALLERY = 'gallery';
    public const SUBSCRIPTION = 'subscription';
    public const CONTACTS = 'contacts';

    /**
     * @var DynamicPageRepository
     */
    private $dynamicPageRepository;


    public function __construct(DynamicPageRepository $dynamicPageRepository)
    {
        $this->dynamicPageRepository = $dynamicPageRepository;
    }


    public static function getArrayModules()
    {
        return [
            'Отключено' => null,
            self::COACH => self::COACH,
            self::GALLERY => self::GALLERY,
            self::EVENTS => self::EVENTS,
            self::NEWS => self::NEWS,
            self::REVIEWS => self::REVIEWS,
            self::SLIDER => self::SLIDER,
            self::SUBSCRIPTION => self::SUBSCRIPTION,
            self::CONTACTS => self::CONTACTS
        ];
    }


    public function getEventRoutes($currentModule){


        $isNotActiveModules = ['Отключено' => null];
        $arrayModule = self::getArrayModules();
        $elemActiveModule = [];
        $activeModules = $this->dynamicPageRepository->getRouteByArrayRoute();
        foreach ($activeModules as $module){
            $elemActiveModule[] = $module['pageModule'];
        }

        foreach ($arrayModule as $module){
            if(!in_array($module,$elemActiveModule) and $module !== null){
                $isNotActiveModules[$module] = $module;
            }
        }

        if ($currentModule){
             $isNotActiveModules[$currentModule] = $currentModule;
        }

        return $isNotActiveModules;

    }

}