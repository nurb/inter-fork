<?php

namespace App\Model\Enumeration;


class ClientTypeEnumeration
{
    public const CORPORATE_CLIENT = 'corporate_client';
    public const PRIVATE_CLIENT = 'private_client';

}