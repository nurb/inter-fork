<?php

namespace App\Repository;

use App\Entity\GalleryModule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GalleryModule|null find($id, $lockMode = null, $lockVersion = null)
 * @method GalleryModule|null findOneBy(array $criteria, array $orderBy = null)
 * @method GalleryModule[]    findAll()
 * @method GalleryModule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GalleryModuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GalleryModule::class);
    }

    /**
     * @param $priority
     * @return GalleryModule
     */

    public function findByPriorityField($priority)
    {
        try {
            return $this->createQueryBuilder('g')
                ->andWhere('g.priority = :priority')
                ->setParameter('priority', $priority)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return GalleryModule
     */

    public function findAllGalleryByDESC()
    {
        return $this->createQueryBuilder('t')
            ->orderBy('t.priority', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function findLastElementPosition()
    {
        try {
            return $this->createQueryBuilder('g')
                ->select('g.priority')
                ->orderBy('g.priority', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


}
