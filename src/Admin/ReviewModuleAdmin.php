<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ReviewModuleAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name', null, array('label' => 'Название'))
            ->add('link', null, array('label' => 'Ссылка'))
            ->add('isActive', null, array('label' => 'Доступность в меню'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name',null, array('label' => 'Название'))
            ->add('link',null, array('label' => 'Ссылка'))
            ->add('isActive',null, array('label' => 'Доступность в меню'))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class,array('label' => 'Название'))
            ->add('isActive', ChoiceType::class,array('label' => 'Доступность на странице','required' => true,
                'choices' => [
                    'Доступно' => true,
                    'Недоступно' => false
                ]
            ))
            ->add('link', TextareaType::class, array('label' => 'Ссылка','attr' => array('class' => 'ckeditor')));

    }
}