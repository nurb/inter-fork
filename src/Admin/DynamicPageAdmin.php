<?php

namespace App\Admin;


use App\Model\Enumeration\PageModulesEnumeration;
use App\Repository\DynamicPageRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\Type\ModelType;


class DynamicPageAdmin extends AbstractAdmin
{

    /**
     * @var string
     */
    private $class;
    /**
     * @var DynamicPageRepository
     */
    private $dynamicPageRepository;

    public function __construct(string $code, string $class, string $baseControllerName, DynamicPageRepository $dynamicPageRepository)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->code = $code;
        $this->class = $class;
        $this->baseControllerName = $baseControllerName;
        $this->dynamicPageRepository = $dynamicPageRepository;
    }


    public function createQuery($context = 'list')
    {
        $proxyQuery = parent::createQuery('list');
        // Default Alias is "o"
        // You can use `id` to hide root element/
        $proxyQuery->where('o.id != 1');
        $proxyQuery->addOrderBy('o.root', 'ASC');
        $proxyQuery->addOrderBy('o.left', 'ASC');

        return $proxyQuery;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('up', $this->getRouterIdParameter() . '/up');
        $collection->add('down', $this->getRouterIdParameter() . '/down');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array('label' => 'Название'))
            ->add('route', null, array('label' => 'Маршрут'))
            ->add('content', null, array('label' => 'Содержание(контент)'))
            ->add('isAvailable', null, array('label' => 'Доступность'))
            ->add('isShowOnMenu', null, array('label' => 'Отображать меню'))
            ->add('pageModule', null, array('label' => 'Модуль страницы'))
            ->add('metaKeywords', null, array('label' => 'Ключевые слова(Метаданные)'))
            ->add('metaDescription', null, array('label' => 'Описание(Метаданные)'))
            ->add('metaTitle', null, array('label' => 'Название(Метаданные)'))
            ->add('metaH1', null, array('label' => 'Заголовок H1(Метаданные)'));

    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            // ->add('id')
            ->addIdentifier('leveledTitle', null, array('label' => 'Название', 'route' => ['name' => 'edit']))
            ->add('route', null, array('label' => 'Маршрут'))
            ->add('pageModule', null, array('label' => 'Модуль страницы'))
            ->add('isAvailable', null, array('label' => 'Опубликовать',
                'editable' => true
            ))
            ->add('isShowOnMenu', null, array('label' => 'Отображать меню',
                'editable' => true
            ))
            ->add('priority', 'actions', array(
                'label' => 'Приоритет',
                'actions' => array(
                    'up' => array(
                        'template' => 'MevSortableTreeBundle:Default:list__action_up.html.twig'
                    ),
                    'down' => array(
                        'template' => 'MevSortableTreeBundle:Default:list__action_down.html.twig'
                    )
                )
            ))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                 //   'create_page' => array(
                 //       'template' => 'SonataAdmin/CRUD/dynamic_page/create.html.twig'
                 //   ),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title', null, array('label' => 'Название'))
            ->add('route', null, array('label' => 'Маршрут'))
            ->add('content', null, array('label' => 'Содержание(контент)'))
            ->add('isAvailable', null, array('label' => 'Доступность'))
            ->add('isShowOnMenu', null, array('label' => 'Отображать меню'))
            ->add('pageModule', null, array('label' => 'Модуль страницы'))
            ->add('metaKeywords', null, array('label' => 'Ключевые слова(Метаданные)'))
            ->add('metaDescription', null, array('label' => 'Описание(Метаданные)'))
            ->add('metaTitle', null, array('label' => 'Название(Метаданные)'))
            ->add('metaH1', null, array('label' => 'Заголовок H1(Метаданные)'));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $subjectId = $this->getRoot()->getSubject()->getId();
        $query = null;
        $currentModule = $this->getRoot()->getSubject()->getPageModule();


        $repository = $this->dynamicPageRepository;
        $pageEnum = new PageModulesEnumeration($repository);

        if ($subjectId) {
            $manager = $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getRepository('App\Entity\DynamicPage');
            $query = $manager->createQueryBuilder('d')
                ->select('d')
                ->where('d.id != ' . $subjectId);
        }

        $formMapper
            ->add('title', TextType::class, array('label' => 'Название'))
            ->add('metaTitle', TextType::class, array('label' => 'Название(Метаданные)'))
            ->add('route', TextType::class, array('label' => 'Маршрут'))
            ->add('metaKeywords', TextType::class, array('label' => 'Ключевые слова(Метаданные)'))
            ->add('metaDescription', TextType::class, array('label' => 'Описание(Метаданные)'))
            ->add('metaH1', TextType::class, array('label' => 'Заголовок H1(Метаданные)'))
            ->add('content', TextareaType::class, array('label' => 'Содержание(контент)', 'attr' => array('class' => 'ckeditor')))
            ->add('isAvailable', ChoiceType::class, array('label' => 'Доступонсть',
                'choices' => [
                    'Доступно' => true,
                    'Недоступно' => false
                ]
            ))
            ->add('isShowOnMenu', ChoiceType::class, array('label' => 'Отображать меню',
                'choices' => [
                    'Доступно' => true,
                    'Недоступно' => false
                ]
            ))
            ->add('pageModule', ChoiceType::class, array('label' => 'Модуль страницы',
                'choices' =>
                    $pageEnum->getEventRoutes($currentModule)

            ))
            ->add('parent', ModelType::class, array('label' => 'Родительская страницы',
                'query' => $query,
                'required' => true, // remove this row after the root element is created
                'btn_add' => false,
                'property' => 'leveledTitle'
            ));
    }
}