<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Route\RouteCollection;

class SiteSettingsAdmin extends AbstractAdmin
{
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('mainPageTitle', TextType::class, array('label' => 'Meta заголовок'))
            ->add('mainPageDescription', TextType::class, array('label' => 'Meta Description'))
            ->add('mainPageTextBlock', TextareaType::class, array('label' => 'Текстовый блок на главной', 'attr' => array('class' => 'ckeditor')))
            ->add('mainSliderTimer', NumberType::class, array('label' => 'Таймер основного слайдера'))
            ->add('coachSliderTimer', NumberType::class, array('label' => 'Таймер слайдера тренеров'))
            ->add('reviewSliderTimer', NumberType::class, array('label' => 'Таймер слайдера отзывов'))
            ->add('videoGallerySliderTimer', NumberType::class, array('label' => 'Таймер слайдера видео галереи'))
            ->add('eventsSliderTimer', NumberType::class, array('label' => 'Таймер слайдера мероприятий'))
            ->add('adminEmail', TextType::class, array('label' => 'Email администратора'))
            ->add('facebookUrl', TextType::class, array('label' => 'Адрес страницы на facebook'))
            ->add('instagramUrl', TextType::class, array('label' => 'Адрес страницы в instagram'))
            ->add('googleUrl', TextType::class, array('label' => 'Адрес страницы в google'))
            ->add('vkontakteUrl', TextType::class, array('label' => 'Адрес страницы в vk'))
            ->add('imageSizeLevel1', TextType::class, array('label' => 'Размер изображения уровень 1'))
            ->add('imageSizeLevel2', TextType::class, array('label' => 'Размер изображения уровень 2'))
            ->add('imageSizeLevel3', TextType::class, array('label' => 'Размер изображения уровень 3'))
            ->add('imageSizeLevel4', TextType::class, array('label' => 'Размер изображения уровень 4'))
            ->add('imageSizeLevel5', TextType::class, array('label' => 'Размер изображения уровень 5'))
        ;
    }
}