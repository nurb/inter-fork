<?php

namespace App\Admin;

use App\Entity\EventModule;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class EventSessionAdmin extends AbstractAdmin
{
    protected $parentAssociationMapping = 'event';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('event', null, array('label' => 'Мероприятие'))
            ->add('date', null, array('label' => 'Дата'))
            ->add('price', null, array('label' => 'Цена'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, array('label' => 'Название', 'route' => ['name' => 'show']))
            ->add('date', 'datetime', array('label' => 'Дата сеанса', 'format' => 'd.m.Y'))
            ->add('session', 'actions', array(
                'label' => 'Список регистраций',
                'actions' => array(
                    array('template' => 'SonataAdmin/CRUD/events/show_booking.html.twig')
                )
            ))
            ->add('eventFiles', 'actions', array(
                'label' => 'Список файлов',
                'actions' => array(
                    array('template' => 'SonataAdmin/CRUD/events/show_files.html.twig')
                )
            ))
            ->add('_action', null, array('label' => 'Действие',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('event.eventName', null, array(
                'label' => 'Мероприятие',
                'attr' => array(
                    'readonly' => true,
                )
            ))
            ->add('name', TextType::class, array('label' => 'Название'))
            // ->add('event', EntityType::class, array('label' => 'Мероприятие', 'class' => EventModule::class))
            ->add('date', DateType::class, array('label' => 'Дата', 'widget' => 'single_text'))
            ->add('price', NumberType::class, array('label' => 'Цена'))
            ->add('eventFiles', null, array('label' => 'Материалы'));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('event', null, array('label' => 'Мероприятие'))
            ->add('date', null, array('label' => 'Дата'))
            ->add('price', null, array('label' => 'Цена'))
            ->add('eventFiles', null, array('label' => 'Материалы'));

    }
}