<?php

namespace App\DataFixtures;


use App\Entity\EventFile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class EventFileFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $eventSession1 = $this->getReference('eventSession1');
        $eventSession2 = $this->getReference('eventSession2');
        $eventSession3 = $this->getReference('eventSession3');
        $eventSession4 = $this->getReference('eventSession4');


        $eventFile1 = new EventFile();
        $eventFile1
            ->setName('Document1')
            ->setEvent($eventSession1)
            ->setMaterial('eventFile1.docx');
        $manager->persist($eventFile1);


        $eventFile2 = new EventFile();
        $eventFile2
            ->setName('Document2')
            ->setEvent($eventSession1)
            ->setMaterial('eventFile2.docx');
        $manager->persist($eventFile2);


        $eventFile3 = new EventFile();
        $eventFile3
            ->setName('Document3')
            ->setEvent($eventSession3)
            ->setMaterial('eventFile3.pdf');
        $manager->persist($eventFile3);


        $eventFile4 = new EventFile();
        $eventFile4
            ->setName('Document4')
            ->setEvent($eventSession4)
            ->setMaterial('eventFile4.pdf');
        $manager->persist($eventFile4);


        $manager->flush();
    }

    public function getDependencies()

    {

        return array(

            EventSessionFixtures::class

        );

    }

}