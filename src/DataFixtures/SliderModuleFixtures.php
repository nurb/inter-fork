<?php

namespace App\DataFixtures;

use App\Entity\SliderModule;
use App\Entity\User;
use App\Model\Enumeration\ClientTypeEnumeration;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SliderModuleFixtures extends Fixture
{

    const SLIDER_ONE = 'slider_1';
    const SLIDER_TWO = 'slider_2';
    const SLIDER_THREE = 'slider_3';

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    )
    {

        $images = [];
        $images[] = self::SLIDER_ONE;
        $images[] = self::SLIDER_TWO;
        $images[] = self::SLIDER_THREE;


    for ($i = 0; $i < count($images); $i++){
        $slider = new SliderModule();

        $slider
            ->setName('Мы вдохновляем на достижения!')
            ->setIsActive(true)
            ->setLink('http://127.0.0.1:8000/page/events')
            ->setText('Деятельность по консультированию руководителей,' .
                'управленцев по широкому кругу вопросов в сфере финансовой, ' .
                'коммерческой, юридической, технологической, технической, ' .
                'экспертной деятельности. Цель консалтинга — помочь системе управления' .
                'Деятельность по консультированию руководителей, управленцев по широкому' .
                'кругу вопросов в сфере финансовой,коммерческой, юридической,' .
                ' технологической, технической, экспертной деятельности.' .
                'Цель консалтинга — помочь системе управления')
            ->setImage($images[$i].'.jpeg');
        $manager->persist($slider);
        }
    $manager->flush();
    }
}
