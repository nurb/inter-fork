<?php

namespace App\DataFixtures;


use App\Entity\VideoGalleryModule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class VideoGalleryModuleFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $names = ['НЛП в Бишкеке. Обучение на курсе НЛП-Практик в КЦ "ИнтерАктив"',
            'Тренинг "Карьерный рост"',
            'Михаил Мунькин. Тренинг "Победить в переговорах"!',
            'Компания Интерактив и Аркадий Бондарь проводят сертификацию НЛПеров в Бишкеке! Начало...'];

        $links = ["<iframe width=\"350\" height=\"250\" src=\"https://www.youtube.com/embed/PJ1KVid6vSw\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>",
            "<iframe width=\"350\" height=\"250\" src=\"https://www.youtube.com/embed/G27E8cRE6Oc\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>",
            "<iframe width=\"350\" height=\"250\" src=\"https://www.youtube.com/embed/xN9XjHRh2PM\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>",
            "<iframe width=\"350\" height=\"250\" src=\"https://www.youtube.com/embed/orJmQAaGLFs\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>"];


        for ($i = 0; $i < 4; $i++) {
            $videoGalleryModule = new VideoGalleryModule();
            $videoGalleryModule
                ->setName($names[$i])
                ->setLink($links[$i])
                ->setIsActive(1);

            $manager->persist($videoGalleryModule);
        }
        $manager->flush();
    }

}