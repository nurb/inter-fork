# language: ru

@eventModule @fixtures
Функционал: Тестируем в админке управления мероприятиями

  @adminPanel @loginAdmin
  Сценарий: Добавление категорий мероприятий в админке
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модуль мероприятий"
    И Я нажимаю на ссылку "Мероприятия"
    И Я нажимаю на ссылку "Добавить новый"
    И я заполняю поля данными
      | Название          | Курс ничего неделания                    |
      | Короткое описание | of the printing and typesetting industry |
      | Дата начала       | 2018-03-02                               |
      | Дата окончания    | 2018-03-02                               |
      | Категории         | pro                                      |
      | Количество мест   | 10                                       |
      | Тип               | Разовое                                  |
      | Тренеры           | Елена Бевзова                            |
      | Отображение       | 1                                        |
    И я добавляю в дирректорию"Изображение" файл "behat_event_1.png"
    И я пишу в поле wysiwyg "Полное описание" значение "Lorem Ipsum is simply dummy text <br/>of the printing and typesetting industry."
    И я нажимаю на кнопку "btn_create_and_edit"
    Тогда я вижу слово "Элемент создан успешно" на странице

  @adminPanel @loginAdmin
  Сценарий: Редактирование мероприятий в админке
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модуль мероприятий"
    И Я нажимаю на ссылку "Мероприятия"
    И Я нажимаю на ссылку "Редактировать"
    И я заполняю поля данными
      | Название          | Курс ленивости |
      | Короткое описание | Lorem Ipsum    |
      | Дата начала       | 2018-05-02     |
      | Дата окончания    | 2018-05-02     |
      | Категории         | business       |
      | Количество мест   | 10             |
      | Тип               | Длительное     |
      | Тренеры           | Михаил Мунькин |
      | Отображение       | 0              |
    И я добавляю в дирректорию"Изображение" файл "behat_event_2.png"
    И я пишу в поле wysiwyg "Полное описание" значение "Lorem Ipsum is simply dummy text."
    И я нажимаю на кнопку "btn_update_and_edit"
    Тогда я вижу слово "Элемент успешно обновлен." на странице

  @adminPanel @loginAdmin
  Сценарий: Редактирование мероприятий в админке
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модуль мероприятий"
    И Я нажимаю на ссылку "Мероприятия"
    И Я нажимаю на ссылку "Удалить"
    И я нажимаю на кнопку "Да, удалить"
    Тогда я вижу слово "Элемент успешно удален." на странице

  @adminPanel @loginAdmin
  Сценарий: Тестируем переход по каскаду таблиц.
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модуль мероприятий"
    И Я нажимаю на ссылку "Мероприятия"
    И Я нажимаю на ссылку "Показать сеансы"
    И я вижу слово "Event Session List" на странице
    И Я нажимаю на ссылку "Показать регистрации"
    И я вижу слово "Event Booking List" на странице


  @adminPanel @loginAdmin
  Сценарий: Отключаем мероприятие в админ панеле
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модуль мероприятий"
    И Я нажимаю на ссылку "Все страницы"
    И Я нажимаю на элемент ".label"
    И Я нажимаю на выпадающий список ".input-sm" и выбираю "0"
    И Я нажимаю на элемент ".glyphicon"
    Тогда я вижу, что на страница не выбросила ошибку с исключением
