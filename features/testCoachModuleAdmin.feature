# language: ru

@coachModule @fixtures
Функционал: Тестируем в админке CoachModule

  @adminPanel @loginAdmin
  Сценарий: Добавление страницы CoachModule в админ панеле
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модули"
    И Я нажимаю на ссылку "Тренеры"
    И Я нажимаю на ссылку "Добавить новый"
    И я заполняю поля данными
      | Фамилия Имя Отчество | Человек Пароход                                                            |
      | Краткое описание     | Lorem Ipsum is simply dummy text of the printing and typesetting industry. |
      | Приоритет            | 1                                                                          |
    И я добавляю в дирректорию"Изображение" файл "behat_coach4.jpg"
    И я пишу в поле wysiwyg "Полное описание" значение "Lorem Ipsum is simply dummy text <br/>of the printing and typesetting industry."
    И Я нажимаю на элемент ".icheckbox_square-blue"
    И я нажимаю на кнопку "btn_create_and_list"
    Тогда я вижу слово "Элемент создан успешно" на странице

  @adminPanel @loginAdmin
  Сценарий: Редактирование страницы CoachModule в админ панеле
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модули"
    И Я нажимаю на ссылку "Тренеры"
    И Я нажимаю на ссылку "Редактировать"
    И я заполняю поля данными
      | Фамилия Имя Отчество | Зинидин Зидан                                                              |
      | Краткое описание     | Lorem Ipsum is simply dummy text of the printing and typesetting industry. |
      | Приоритет            | 1                                                                          |
    И я добавляю в дирректорию"Изображение" файл "behat_client4.jpg"
    И я пишу в поле wysiwyg "Полное описание" значение "Lorem Ipsum is simply dummy text <br/>of the printing and typesetting industry."
    И Я нажимаю на элемент ".icheckbox_square-blue"
    И я нажимаю на кнопку "btn_update_and_edit"
    Тогда я вижу слово "Элемент успешно обновлен." на странице

  @adminPanel @loginAdmin
  Сценарий: Удалeние страницы CoachModule в админ панеле
    Допустим я перехожу в панель администратора
    И Я нажимаю на ссылку "Модули"
    И Я нажимаю на ссылку "Тренеры"
    И Я нажимаю на ссылку "Удалить"
    И я нажимаю на кнопку "Да, удалить"
    Тогда я вижу слово "Элемент успешно удален." на странице
