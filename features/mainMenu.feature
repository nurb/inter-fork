# language: ru

@mainMenu @fixtures
Функционал: Тестируем основное меню навигации

  @frontEnd
  Структура сценария: Проход по всем ссылкам меню навигации
    Допустим я нахожусь на главной странице
    И Я нажимаю на элемент "<element_name>"
    Тогда Я вижу cлово "<title>" в элементе "h1"
    Примеры:
      | title        | element_name      |
      | Новости      | #menu-news        |
      | Отзывы       | #menu-review      |
      | Наши тренеры | #menu-coach       |
      | О нас        | #menu-about-us    |
      | Для компании | #menu-for-company |
      | Мероприятия  | #menu-events       |
      | Материалы    | #menu-material    |
      | Фотогалерея  | #menu-gallery     |
      | Контакты     | #menu-contacts    |


